## A simple chart with a statefulset and headless service

 - headless (i.e. service with clusterIP: None) service 
 - a statefulset (Replicas: 3) using the [containeous/whoami](https://hub.docker.com/r/containous/whoami) image
